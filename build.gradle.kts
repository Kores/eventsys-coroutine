import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.github.hierynomus.license") version "0.16.1"
    kotlin("jvm") version "1.5.30"
    id("java")
    id("maven-publish")
    application
}

group = "com.koresframework"
version = "1.1.0-rc4"

java {
    sourceCompatibility = JavaVersion.VERSION_16
    targetCompatibility = JavaVersion.VERSION_16
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/28895078/packages/maven")
    // BytecodeDisassembler
    maven("https://gitlab.com/api/v4/projects/28905629/packages/maven")

    // Kores
    maven("https://gitlab.com/api/v4/projects/28894889/packages/maven")
    maven("https://gitlab.com/api/v4/projects/28905266/packages/maven")
    maven("https://gitlab.com/api/v4/projects/28905428/packages/maven")
    maven("https://gitlab.com/api/v4/projects/29289915/packages/maven")
    maven("https://gitlab.com/api/v4/projects/29384742/packages/maven")
    maven("https://gitlab.com/api/v4/projects/29386105/packages/maven")
}

dependencies {
    // Kores
    implementation("com.koresframework:kores:4.2.8.base")
    implementation("com.koresframework:kores-bytecodewriter:4.2.10.bytecode")
    implementation("com.koresframework:kores-sourcewriter:4.2.4.source")
    implementation("com.github.jonathanxd:kores-extra:1.6.0")

    api("com.github.jonathanxd:jwiutils:4.17.8")
    api("com.github.jonathanxd:jwiutils-kt:4.17.8")
    api("com.github.jonathanxd:specializations:4.17.8")

    implementation("com.koresframework:koresproxy:2.8.2")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")
    implementation("com.github.koresframework:eventsys:2.0.0-rc7")

    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "16"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=all")
}

tasks {
    withType<nl.javadude.gradle.plugins.license.License> {
        header = rootProject.file("LICENSE")
        strictCheck = true
    }
}

val sourcesJar = tasks.create<Jar>("sourcesJar") {
    dependsOn("classes")
    this.archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    repositories {
        maven {
            // change to point to your repo, e.g. http://my.org/repo
            url = uri("$buildDir/repo")
        }
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/29386413/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
        maven {
            name = "GitLabJgang"
            url = uri("https://gitlab.com/api/v4/projects/30392813/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        register("maven", MavenPublication::class) {
            from(components["kotlin"])
            artifactId = "eventsys-coroutine"
            artifact(sourcesJar)
        }
    }
}